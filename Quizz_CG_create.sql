-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Mar 28 Mars 2017 à 23:27
-- Version du serveur :  8.0.0-dmr-log
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `quizz_cg_create`
--

-- --------------------------------------------------------

--
-- Structure de la table `reponsetrue`
--

CREATE TABLE `reponsetrue` (
  `id` int(11) NOT NULL,
  `reponse` varchar(255) NOT NULL,
  `Questions_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `reponsetrue`
--

INSERT INTO `reponsetrue` (`id`, `reponse`, `Questions_id`) VALUES
(1, 'Sucre', 1),
(2, '2', 2),
(3, '1982', 3),
(4, 'Leonardo Di Caprio', 4),
(5, '9', 5),
(6, '55', 6),
(7, '1976', 7),
(8, 'Blanc', 8),
(9, 'Émile Reynaud', 9);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `reponsetrue`
--
ALTER TABLE `reponsetrue`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Questions_id` (`Questions_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `reponsetrue`
--
ALTER TABLE `reponsetrue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
