#!/usr/bin/env node

var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var mysql = require('mysql');

// On gère les requêtes HTTP des utilisateurs en leur renvoyant les fichiers du dossier 'public'
app.use("/", express.static(__dirname + "/public"));

// On lance le serveur en écoutant les connexions arrivant sur le port 3000
http.listen(3000, function(){
  console.log('Server is listening on *:3000');
});

var connection = mysql.createConnection({
        host     : 'localhost',
        user     : 'projet',
        password : 'projet',
        database : 'Quizz_CG_create'
});

connection.connect();

var genre;


function getQuestion (genreId, callback) {
	/*Déclaration des variables*/
	var question,
		question_id,
		reponse1,
		reponse2,
		reponse3,
		reponse4,
		questionObject = {};
		
		/*Récupération des données dans la base de données*/
		connection.query('SELECT * FROM questions WHERE Genre_id='+genreId, function( error, result, field ) {
			var l= result.length;
			var nbAleatoire = Math.floor(Math.random()*l);
			question = result[nbAleatoire].question;
			question_id = result[nbAleatoire].id;
			connection.query('SELECT * FROM reponsetrue WHERE Questions_id=' + question_id, function ( error, result, field ) {
				reponse1=result[0].reponse;
				connection.query('SELECT * FROM reponsefalse WHERE Questions_id='+ question_id, function( error, result, field ) {
					reponse2=result[0].reponse;
					reponse3=result[1].reponse;
					reponse4=result[2].reponse;
					questionObject = { question: question, reponse :[ reponse1, reponse2, reponse3, reponse4]};
					if (callback) {
					callback(questionObject)};
				});
			});
		});
};

function getScore(callback){
	connection.query('SELECT Id, Score FROM users', function(error, result, field){
		var nbUsers = result.length;
		var scoreObject = {};
		var scoreArray= new Array();
		for (var i =0; i <nbUsers; i++) {
			scoreArray[i]=new Array();			
		};
		for (var i=0; i< nbUsers;i++){
			scoreArray[i][0]=result[i].Id;
			scoreArray[i][1]=result[i].Score;
		};
		scoreObject={nbUsers : nbUsers, scoreArray : scoreArray};
		if (callback){
		callback(scoreObject)};
	});
};


io.on('connection', function (socket) {   // réception événément 'connection'
// 	console.log('a user connected');
	var socketId = socket.id,
		score = 0,
		genre,
		timerRunning=false,
		timerPaused=true,
		time = 15;
	connection.query("INSERT INTO Users (Socket, Score) VALUES ( '"+ socketId +"', " + score + ")");
        connection.query('SELECT genre FROM genre', function (error, result, field){
		socket.emit('hello', result); // émission  événement 'hello'
        });
	
	socket.on('scoreAsked',function(){
		getScore(function(scoreObject){
			console.log(scoreObject.nbUsers);
			console.log(scoreObject.scoreArray);
			io.sockets.emit('newScore', JSON.stringify(scoreObject));
		});
	});
	
	socket.on('genreSelected',function(genreId){
		time = 15;
		resetTimer();
		genre = genreId;
		console.log('genre'+genreId+' selectionne');
		getQuestion(genreId, function(questionObject){
			io.sockets.emit('newQuestion', JSON.stringify(questionObject));
		});
	});

	socket.on('rightAnswer', function(){
		var nscore;
		pauseTimer();
		connection.query("SELECT Score FROM Users WHERE Socket='" + socket.id + "'", function(error, result, field){
			console.log(result[0].Score);
			nscore = result[0].Score +1;
			connection.query("UPDATE Users SET Score=" + nscore + " WHERE Socket='" + socket.id + "'");
			socket.emit('score', nscore);
			io.sockets.emit('found', '');
		});
	});

	socket.on('disconnect', function() {
		connection.query("DELETE FROM Users WHERE Socket = '" + socket.id + "'");
	});
	
	function resetTimer(){
		timerPaused=false;
		if(!timerRunning){
			timer();
		}
	};

	function timer() {
		if(!timerPaused){
		if(time <= 0){
			timerRunning = false;
			console.log(time);
			time=15;
			getQuestion(genre, function(questionObject){
				io.sockets.emit('newQuestion', JSON.stringify(questionObject));
			});
			resetTimer();
		} else {
			timerRunning = true;
			time = time - 1;
			setTimeout(function() {
				timer()
			}, 1000);
		}
		}
	};

	function pauseTimer(){
		timerPaused = true;
		if(timerRunning){
			timerRunning = false;
		}
	};

	
});
